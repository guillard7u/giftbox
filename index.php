<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 12/12/2016
 * Time: 11:10
 */
define("LIST_VIEW", 1);
define("PRESTA_VIEW", 2);
define("LIST_CATEGORIES_VIEW", 3);
define("LIST_PRESTATION_CATEGORIE_VIEW", 4);
//define("COFFRET_NULL_VIEW", 1);
//define("COFFRET_VIEW", 2);
define("PATH", "/guillard7u/PHP/giftbox");

require_once 'vendor/autoload.php';

//$iniCo = parse_ini_file('src/conf/conf.ini');

\giftbox\controllers\ConnectionController::connection();

$app = new \Slim\Slim;

$app->get('/', function () {
    //print "Page d'accueil test";
    \giftbox\controllers\AccueilController::afficherPageAccueil();

});


$app->get('/prestation', function () {

    \giftbox\controllers\CatalogueController::afficherListePrestations();
});

$app->get('/prestation/:id', function ($id) {
    \giftbox\controllers\CatalogueController::afficherUnePrestation($id);
});


$app->get('/categorie', function () {
    \giftbox\controllers\CatalogueController::afficherCategories();
});

$app->get('/categorie/:id', function ($id) {
    \giftbox\controllers\CatalogueController::afficherContenuCategories($id);
});

// TODO finir après l'ajout de variables de session
$app->get('/coffret', function () {
    \giftbox\controllers\CoffretController::afficherCoffret();
});

$app->post('/coffretAll/:id', function ($id) {
    \giftbox\controllers\CoffretController::afficherCoffretAll($id);
});

$app->post('/coffretOne/:id', function ($id) {
    \giftbox\controllers\CoffretController::afficherCoffretOne($id);
});

$app->get('/coffret/:id', function ($id ) {
    \giftbox\controllers\CoffretController::afficherCoffretOffert($id);
});

$app->get('/suivi/:id', function ($id) {
    \giftbox\controllers\CoffretController::afficherSuivi($id);
});

//$app->get('/coffretaoffrir', function () {
//    \giftbox\controllers\CoffretAOffrirController::afficherCoffretAOffrir();
//});

$app->post('/ajoutCoffret', function () {
    if (isset($_POST['idPresta'])) {
        \giftbox\controllers\CoffretController::ajouterPrestation($_POST['idPresta']);

    }
    \giftbox\controllers\CoffretController::afficherCoffret();
});

$app->post('/validationCoffret', function () {
    \giftbox\controllers\ValidationCoffretController::afficher();
});

$app->post('/payement', function () {
    \giftbox\controllers\PayementController::afficherFormulaireCB();
});

$app->get('/ajoutPrestationCoffret/:id', function ($id) {
    \giftbox\controllers\CoffretController::ajouterPrestation($id);
    $pcl = \giftbox\models\PrestaCoffret::all();
    foreach ($pcl as $pc) {
        print $pc->id . ' ' . $pc->coffret_id . ' ' . $pc->presta_id . '</br>';
    }
});

$app->post('/suppressionPrestation', function () {
    if (isset($_POST['idPresta'])) {

        if (isset($_POST['newQuantity'])) {
            // enlève les espaces
            $text = preg_replace("/\s+/", "", $_POST['newQuantity']);
            if (preg_match("/^[0-9]{1}$/", $text)) {
                \giftbox\controllers\CoffretController::changerNbPrestation($_POST['idPresta'], $text);
            }
        } else {
            \giftbox\controllers\CoffretController::supprimerPrestation($_POST['idPresta']);
        }

    }


    \giftbox\controllers\CoffretController::afficherCoffret();
});


$app->post('/suppressionPrestationTotale', function () {
    if (isset($_POST['idPresta'])) {
        \giftbox\controllers\CoffretController::supprimerPrestationTotale($_POST['idPresta']);

    }


    \giftbox\controllers\CoffretController::afficherCoffret();
});

$app->post('/valide', function () {
    \giftbox\controllers\CoffretController::validerCoffret();
    print "merci" . '</br>';
    \giftbox\controllers\GenerationUrlController::genererUrlCoffret();
});


$app->get('/croissant', function () {

    \giftbox\controllers\CatalogueController::afficherListePrixCroissant();
});

$app->get('/decroissant', function () {

    \giftbox\controllers\CatalogueController::afficherListePrixDecroissant();
});


$app->get('/croissant/:id', function ($id) {

    \giftbox\controllers\CatalogueController::afficherListePrixCroissantCategorie($id);
});

$app->get('/decroissant/:id', function ($id) {

    \giftbox\controllers\CatalogueController::afficherListePrixDecroissantCategorie($id);
});

$app->get('/cagnotte', function () {

    \giftbox\controllers\CagnotteController::afficherCagnotte();
});

$app->get('/remplircagnotte', function () {

    \giftbox\controllers\CagnotteController::remplirCagnotte();
});

$app->get('/liencagnotte', function () {

    \giftbox\controllers\CagnotteController::lienCagnotte();
});



$app->run();









