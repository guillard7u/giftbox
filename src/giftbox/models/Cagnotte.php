<?php

namespace giftbox\models;
use Illuminate\Database\Capsule\Manager as DB;
$iniCo = parse_ini_file('src/conf/conf.ini');
/** @noinspection PhpUndefinedNamespaceInspection */


class Cagnotte extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'cagnotte';
	protected $primaryKey = 'id';
	public $timestamps = 'false';
    protected $fillable = ['id','nom', 'particip'];



}



//TABLE CAGNOTTE
/**
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
*/

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

/**
--
-- Base de données :  `giftbox`
--

-- --------------------------------------------------------

--
-- Structure de la table `cagnotte`
--

CREATE TABLE `cagnotte` (
`id` int(3) DEFAULT NULL,
  `nom` varchar(20) DEFAULT NULL,
  `particip` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 * */
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
