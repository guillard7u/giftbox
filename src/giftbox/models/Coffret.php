<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 05/12/2016
 * Time: 11:17
 */

namespace giftbox\models;


// TODO utiliser les variables de session pour savoir de quel coffret il s'agit
// TODO revoir la manière dont coffret est crée pour récupérer l'id du coffret
/** @noinspection PhpUndefinedNamespaceInspection */
class Coffret extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'coffret';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['prix', 'message', 'nom', 'prenom', 'payement', 'etat', 'mail'];

    // Il faut lier avec prestaCoffret pour récupérer les prestations qui correspondent à un coffret


    public function prestaCoffret()
    {
        return $this->hasMany('\giftbox\models\PrestaCoffret', 'coffret_id');
    }
}