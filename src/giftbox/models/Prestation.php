<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 05/12/2016
 * Time: 11:17
 */

namespace giftbox\models;
use /** @noinspection PhpUndefinedNamespaceInspection */
    Illuminate\Database\Capsule\Manager as DB;
$iniCo = parse_ini_file('src/conf/conf.ini');

/** @noinspection PhpUndefinedNamespaceInspection */
class Prestation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'prestation';
    protected $primaryKey = 'id';
    public $timestamps = 'false';

    public function categorie()
    {
        return $this->belongsTo('\giftbox\models\Categorie', 'cat_id');
    }

    public function notes()
    {
        return $this->hasMany('\giftbox\models\Notes', 'presta_id');
    }


}