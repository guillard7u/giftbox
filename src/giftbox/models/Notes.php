<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 05/12/2016
 * Time: 11:17
 */

namespace giftbox\models;
use Illuminate\Database\Capsule\Manager as DB;
$iniCo = parse_ini_file('src/conf/conf.ini');

class Notes extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'notes';
    protected $primaryKey = 'id';
    public $timestamps = 'false';
    protected $fillable = ['id', 'notes'];

    public function prestation()
    {
        return $this->belongsTo('\giftbox\models\Prestation', 'presta_id');
    }


}