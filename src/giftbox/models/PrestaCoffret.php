<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 22/12/2016
 * Time: 19:06
 */

namespace giftbox\models;

/** @noinspection PhpUndefinedNamespaceInspection */
class PrestaCoffret extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'prestacoffret';
    protected $primaryKey = 'id';
    protected $fillable = array('coffret_id', 'presta_id', 'prix');
    public $timestamps = false;


    public function prestations()
    {
        return $this->hasMany('\giftbox\models\Prestation', 'presta_id');
    }

    public function coffret()
    {
        return $this->belongsTo('\giftbox\models\Coffret', 'coffret_id');
    }
}