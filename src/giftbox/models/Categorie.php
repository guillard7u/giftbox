<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 05/12/2016
 * Time: 11:17
 */

namespace giftbox\models;
use /** @noinspection PhpUndefinedNamespaceInspection */
    Illuminate\Database\Capsule\Manager as DB;
$iniCo = parse_ini_file('src/conf/conf.ini');

/** @noinspection PhpUndefinedNamespaceInspection */
class Categorie extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'categorie';
    protected $primaryKey = 'id';
    public $timestamps = 'false';


    public function prestations()
    {
        return $this->hasMany('\giftbox\models\Prestation', 'cat_id');
    }
}