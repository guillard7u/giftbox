<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 25/12/2016
 * Time: 00:10
 */

namespace giftbox\view;

// TODO finir
use giftbox\models\Coffret;
use giftbox\models\PrestaCoffret;

class VueCoffret
{
    protected $selecteur = -1;
    protected $liste = array();
    protected $quantite = array();

    protected $nom;
    protected $prenom;
    protected $message;
    protected $id;


    function __construct($tab = null, $q = null, $id = null)
    {
        $this->id = $id;
        $this->liste = $tab;
        $this->quantite = $q;
        if (isset($_SESSION['nom'])) {
            $this->nom = $_SESSION['nom'];
        }
        if (isset($_SESSION['prenom'])) {
            $this->prenom = $_SESSION['prenom'];
        }
        if (isset($_SESSION['commentaire'])) {
            $this->message = $_SESSION['commentaire'];
        }
    }


    public function render($selec)
    {
        $this->selecteur = $selec;
        $content = "";
        //$swap = '/giftbox';
        $swap = "./";
        switch ($this->selecteur) {
            case COFFRET_NULL_VIEW : {
                $content = $this->htmlCoffretVide();
                $swap = "";
                break;
            }
            case COFFRET_VIEW : {
                $content = $this->htmlCoffret();
                $swap = "";
                break;
            }
            case COFFRET_CATE_VIEW: {
                $content = $this->htmlCoffretCategorie();
                $swap = "";
                break;
            }
            case COFFRET_OFFERT_VIEW: {
                $content = $this->htmlCoffretOffert();
                $swap = "/..";
                break;
            }
            case COFFRET_CHOIX_VIEW : {
                $swap = "/..";
                $content = $this->htmlCoffret();
                break;
            }
            case COFFRET_ALL_VIEW : {
                $swap = "/..";
                $content = $this->htmlCoffretAll();
                break;
            }
            case COFFRET_ONE_VIEW : {
                $swap = "/..";
                $content = $this->htmlCoffretOne();
                break;
            }
            case COFFRET_SUIVI_VIEW : {
                $swap = "/..";
                $content = $this->htmlCoffretSuivi();
                break;
            }
        }

        $html = '
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    <title>Giftbox</title>
                    <link href=".' . $swap . '/web/css/styles.css" rel="stylesheet" type="text/css" />
                    <link rel="stylesheet" href=".' . $swap . '/web/css/boot.css">
                    
                    
                    <!--<link rel="stylesheet" href="./web/css/bootstrap.css">
                    <link rel="stylesheet" href="./web/css/bootstrap-theme.css">-->
                
                    
                    <link href=\'http://fonts.googleapis.com/css?family=Playfair+Display\' rel=\'stylesheet\' type=\'text/css\'>
                    <link href=\'http://fonts.googleapis.com/css?family=Lato\' rel=\'stylesheet\' type=\'text/css\'>
                </head>
                <body>
                <div class="header-section">
                  <div class="header">
                    <div class="logo">
                      <a href="./"><h1>GiftBox</h1></a>
                    </div>
                    <div class="menu">
                      <ul>
                        <li><a href=".' . $swap . '/">Accueil</a></li>
                        <li><a href=".' . $swap . '/prestation">Catalogue</a></li>                        
                        <li><a href=".' . $swap . '/categorie">Catégories</a></li>
                        <li><a href=".' . $swap . '/coffret">Coffret</a></li>
                        <li><a href=".' . $swap . '/cagnotte">Cagnotte</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                    <div class="clear"></div>
                											
                											
                											    ' . $content . '
                											    
                											    
                	</div>				
                				<div class="clear"></div>
                <div class="footer-section">
                  <div class="footer">
                    <div class="panel marRight30">
                      <div class="title">
                        <h2>Liens utiles</h2>
                      </div>
                      <div class="content">
                        <ul>
                             <li><a href=".' . $swap . '/">Accueil</a></li>
                            <li><a href=".' . $swap . '/prestation">Catalogue</a></li>
                            <li><a href=".' . $swap . '/coffret">Coffret</a></li>
                            <li><a href=".' . $swap . '/cagnotte">Cagnotte</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="panel marRight30">
                      <div class="title">
                        <h2>A propos</h2>
                      </div>
                      <div class="content">
                        <P><span>Bienvenur sur Giftbox</span></P>
                        <P>Nous vous présentons tout un catalogue de prestation</P>
                        <P>Vous pouvez créer vos coffret et les envoyez à vos amis</P>
                      </div>
                    </div>
                    <div class="panel">
                      <div class="title">
                        <h2>Copy Rights</h2>
                      </div>
                      <div class="content">
                        <div class="copyriight">
                          <P class="border-bottom">Giftbox <br />
                            Copy rights. 2017. All rights reserved.</P>
                          <P>Designed By : <a href="www.alltemplateneeds.com" target="_blank">www.alltemplateneeds.com</a><br />
                            Image courtesy.<a href="www.photorack.net" target="_blank"> www.photorack.net</a></P>
                        </div>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>
                </div>
                <!---------------end-footer-section---------------->
                </body>
                </html>
                ';
        print $html;
    }


    private function htmlCoffretVide()
    {
        $html = '<h1>Votre coffret est vide. Ajoutez une prestation au coffret pour que ce 
                dernier se remplisse.</h1>';  // TODO Régler le problème de formatage


        return $html;
    }


    private function htmlCoffret()
    {
        $html = '<section>
                    <h1>Contenu du coffret</h1>
                    <table style="width:100%">
                      <tr>
                        <th>Nom</th> 
                        <th>Catégorie</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Quantité</th>
                        <th>Prix</th>
                      </tr>';
        $i = 0;
        $prixTot = 0;
        foreach ($this->liste as $list) {
            $html .= '<tr>
                        <td>' . $list->nom . '</td>
                        
                        <td>
                            <a href = "categorie/' . $list->categorie->id . '">
                                   ' . $list->categorie->nom . '
                            </a>
                        </td>
                        <td>' . $list->descr . '</td>
                        <td>
                            <a href="prestation/' . $list->id . '">
                                <img src=./web/img/' . $list->img . ' alt="Image" style="width:304px;height:228px;">
                            </a>
                        </td>
                        <td>
                            <span title="Entrez une valeur comprise entre 0 et 9">
                                <form method="post" action = "suppressionPrestation">
                                    <input type="hidden" name="idPresta" value="' . $list->id . '" id="id"  />
                                    <input type = "text" name = "newQuantity" value="' . $_SESSION['panier'][$list->id] . ' "size="2"/>
                                    <input type = "submit" value="Changer la quantité" />
                                </form>
                            </span>
                        </td>
                        <td>' . $list->prix . '€</td>
                        
                        
                        <td>
                            <form method="post" action="suppressionPrestationTotale">
                                <input type="hidden" name="idPresta" value="' . $list->id . '" id="id"  />
                                <input id="supprimerPrestationTotale" type="submit" name="" value="Supprimer la prestation" />
                            </form>
                        </td>
                    </tr>';
            $prixTot += $this->quantite[$i] * $list->prix;
            $i++;
        }


        $html .= '    </table>
                  <br>
                  <p align="right">Prix du coffret : ' . $prixTot . '€</p>
                  <div align="right">
                  	<form method="post" action = "validationCoffret">
                  	    <input type="hidden" name="input" value="validerCoffret" />
                  		<input type="submit" value="Valider le coffret"/>
                  	</form>
                  	<!--<p>Envoyez votre coffret à vos amis ou à votre Famille !</p>
                  	<a href="/coffretaoffrir">URL Coffret à offir</a>-->
                  </div>
                  </section>';

        return $html;
    }


    private function htmlCoffretCategorie()
    {
        $html = '<h2>Votre coffret doit contenir des prestations d\'au moins deux catégories différentes.</h2>
				<section>
                    <h1>Contenu du coffret</h1>
                    <table style="width:100%">
                      <tr>
                        <th>Nom</th> 
                        <th>Catégorie</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Quantité</th>
                        <th>Prix</th>
                      </tr>';
        $i = 0;
        $prixTot = 0;
        foreach ($this->liste as $list) {
            $html .= '<tr>
                        <td>' . $list->nom . '</td>
                        
                        <td>
                            <a href = "categorie/' . $list->categorie->id . '">
                                   ' . $list->categorie->nom . '
                            </a>
                        </td>
                        <td>' . $list->descr . '</td>
                        <td>
                            <a href="prestation/' . $list->id . '">
                                <img src=./web/img/' . $list->img . ' alt="Image" style="width:304px;height:228px;">
                            </a>
                        </td>
                        <td>
                            <span title="Entrez une valeur comprise entre 0 et 9">
                                <form method="post" action = "suppressionPrestation">
                                    <input type="hidden" name="idPresta" value="' . $list->id . '" id="id"  />
                                    <input type = "text" name = "newQuantity" value="' . $_SESSION['panier'][$list->id] . ' " size="2"/>
                                    <input type = "submit" value="Changer la quantité" />
                                </form>
                            </span>
                        </td>
                        <td>' . $list->prix . '€</td>
                        
                        
                        <td>
                            <form method="post" action="suppressionPrestationTotale">
                                <input type="hidden" name="idPresta" value="' . $list->id . '" id="id"  />
                                <input id="supprimerPrestationTotale" type="submit" name="" value="Supprimer la prestation" />
                            </form>
                        </td>
                    </tr>';
            $prixTot += $this->quantite[$i] * $list->prix;
            $i++;
        }


        $html .= '    </table>
                  <br>
                  <p align="right">Prix du coffret : ' . $prixTot . '€</p>
                  <div align="right">
                  	<form method="post" action = "validationCoffret">
                  	    <input type="hidden" name="input" value="validerCoffret" />
                  		<input type="submit" value="Valider le coffret"/>
                  	</form>
                  </div>
                  </section>';

        return $html;
    }


    private function htmlCoffretOffert()
    {
        $html = '<section>
                    <h1>Contenu du coffret offert par ' . $this->prenom . ' ' . $this->nom . '</h1>';

        if ($this->message != null) {
            $html .= '<h4 style="text-align: center">"' . $this->message . '"</h4> <br>';
        }
        $html .= '<div style="text-align: center;"> Veuillez choisir la manière dont vous souhaitez voir les prestations votre coffret : 
                <br> 
                <br> 
                    <form method="post" action = "../coffretAll/' . $this->id . '">
                  	    <input type="hidden" name="input" value="coffretAll" />
                  		<input type="submit" value="Toutes à la fois"/>
                  	</form>
                <br> 
                    <form method="post" action = "../coffretOne/' . $this->id . '">
                  	    <input type="hidden" name="input" value="coffretOne" />
                  		<input type="submit" value="Une par une"/>
                  	</form>
                </div>
                <br>';


        $html .= '</section>';
        return $html;
    }


    private function htmlCoffretAll()
    {
        $html = '<section>
                    <h1>Contenu du coffret</h1>
                    <table style="width:100%">
                      <tr>
                        <th>Nom</th> 
                        <th>Catégorie</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Quantité</th>
                      </tr>';
        $i = 0;
        $prixTot = 0;
        foreach ($this->liste as $list) {
            $prestaCoffret = PrestaCoffret::where(['presta_id' => $list->id, 'coffret_id' => $this->id])->first();
            $html .= '<tr>
                        <td>' . $list->nom . '</td>
                        
                        <td>' . $list->categorie->nom . '</td>
                        
                        <td>' . $list->descr . '</td>
                        <td>
                            <img src=./../web/img/' . $list->img . ' alt="Image" style="width:304px;height:228px;">
                        </td>
                        <td>
                                    ' . $prestaCoffret->quantite . '
                        </td>
                        
                        
                        
                    </tr>';
            $prixTot += $this->quantite[$i] * $list->prix;
            $i++;
        }


        $html .= '    </table>
                  </section>';

        return $html;
    }

    private function htmlCoffretOne()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        $html = '<section>
                    <h1>Contenu du coffret</h1>
                    <table style="width:100%">
                      <tr>
                        <th>Nom</th> 
                        <th>Catégorie</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Quantité</th>
                      </tr>';

        if (count($_SESSION['prestationsCoffretOffert'] > 0)) {
            $keys = array_keys($_SESSION['prestationsCoffretOffert']);

            if(!empty($_SESSION['prestationsCoffretOffert'])) {
                $list = $_SESSION['prestationsCoffretOffert'][$keys[0]];
                unset($_SESSION['prestationsCoffretOffert'][$keys[0]]);
            }

            $prestaCoffret = PrestaCoffret::where(['presta_id' => $list->id, 'coffret_id' => $this->id])->first();
            $html .= '<tr>
                        <td>' . $list->nom . '</td>

                        <td>' . $list->categorie->nom . '</td>

                        <td>' . $list->descr . '</td>
                        <td>
                            <img src=./../web/img/' . $list->img . ' alt="Image" style="width:304px;height:228px;">
                        </td>
                        <td>
                                    ' . $prestaCoffret->quantite . '
                        </td>



                    </tr>
                    </table>
                    ';

            $html .= '<div style="text-align: center;">
                    <form method="post" action = "./'.$this->id.'">
                  	    <input type="hidden" name="input" value="coffretAllRefresh" />
                  		<input type="submit" value="Prestation suivante"/>
                  	</form>
                
                </div>';
        } else {
            print 'inf 0<br>';
        }


        $html .= '    
    
                  </section>';

        return $html;
    }

    private function htmlCoffretSuivi()
    {
        $coffret = Coffret::where(['id' => $this->id])->first();
        $html = '<section>
                    <h1>Etat du coffret</h1>
                    <table style="width:100%">
                      <tr>
                        <th>Id du coffret</th> 
                        <th>Prix du coffret</th>
                        <th>État</th>
                      </tr>
                      <tr>
                        <td>' . $coffret->id . '</td>

                        <td>' . $coffret->prix .'€</td>

                        <td>' . $coffret->etat . '</td>
                        </table>';

        return $html;
    }
}