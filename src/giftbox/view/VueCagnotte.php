<?php
/**
 * Created by PhpStorm.
 * User: mathc
 * Date: 18/01/2017
 * Time: 14:40
 */

namespace giftbox\view;


use giftbox\controllers\CagnotteController;
use giftbox\models\Cagnotte;

class VueCagnotte
{
    protected $selecteur = -1;
    protected $swap="";
    protected $liste = array();

    function __construct($tab)
    {
        $this->liste = $tab;
    }

    public function render($selec){
        $this->selecteur = $selec;
        $content = "";
        $swap = "./";
        switch ($this->selecteur) {
            case CAGNOTTE_VIDE : {
                $content = $this->htmlCagnotteVide();
                $swap = "";
                break;
            }
            case REMPLIR_CAGNOTTE : {
                $content = $this->htmlRemplirCagnotte();
                $swap = "";
                break;
            }
            case LIEN_CAGNOTTE : {
                $content = $this->htmlLienCagnotte();
                $swap = "";
                break;
            }

        }

        $html = '
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Giftbox</title>
                
                <!--<link rel="stylesheet" href="./web/css/bootstrap.css">-->
                <link href=".' . $swap . '/web/css/boot.css" rel="stylesheet" type="text/css" />
                <link href=".' . $swap . '/web/css/styles.css" rel="stylesheet" type="text/css" />
                
                <link rel="stylesheet" href=".' . $swap . '/web/css/style.css">
                
                <!--<link rel="stylesheet" href="./web/css/bootstrap-theme.css">-->
                <!--<link rel="stylesheet" href="./web/css/accueil.css">-->
                
                <link href=\'http://fonts.googleapis.com/css?family=Playfair+Display\' rel=\'stylesheet\' type=\'text/css\'>
                <link href=\'http://fonts.googleapis.com/css?family=Lato\' rel=\'stylesheet\' type=\'text/css\'>
                
                </head>
                <body>
                <div class="header-section">
                  <div class="header">
                    <div class="logo">
                      <a href=".' . $swap . '/"><h1>GiftBox</h1></a>
                    </div>
                    <div class="menu">
                      <ul>
                        <li><a href=".' . $swap . '/">Accueil</a></li>
                        <li><a href=".' . $swap . '/prestation">Catalogue</a></li>
                        <li><a href=".' . $swap . '/categorie">Catégories</a></li>
                        <li><a href=".' . $swap . '/coffret">Coffret</a></li>
                        <li><a href=".' . $swap . '/cagnotte">Cagnotte</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                    <div class="clear"></div>
                											
                											
                											    ' . $content . '
                											    
                											    
                	</div>				
                				<div class="clear"></div>
                <div class="footer-section">
                  <div class="footer">
                    <div class="panel marRight30">
                      <div class="title">
                        <h2>Liens utiles</h2>
                      </div>
                      <div class="content">
                        <ul>
                             <li><a href=".' . $swap . '/">Accueil</a></li>
                            <li><a href=".' . $swap . '/prestation">Catalogue</a></li>
                            <li><a href=".' . $swap . '/categorie">Catégories</a></li>
                            <li><a href=".' . $swap . '/coffret">Coffret</a></li>
                            <li><a href=".' . $swap . '/cagnotte">Cagnotte</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="panel marRight30">
                      <div class="title">
                        <h2>A propos</h2>
                      </div>
                      <div class="content">
                        <P><span>Bienvenur sur Giftbox</span></P>
                        <P>Nous vous présentons tout un catalogue de prestation</P>
                        <P>Vous pouvez créer vos coffret et les envoyez à vos amis</P>
                      </div>
                    </div>
                    <div class="panel">
                      <div class="title">
                        <h2>Copy Rights</h2>
                      </div>
                      <div class="content">
                        <div class="copyriight">
                          <P class="border-bottom">Giftbox <br />
                            Copy rights. 2017. All rights reserved.</P>
                          <P>Designed By : <a href="www.alltemplateneeds.com" target="_blank">www.alltemplateneeds.com</a><br />
                            Image courtesy.<a href="www.photorack.net" target="_blank"> www.photorack.net</a></P>
                        </div>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>
                </div>
                <!---------------end-footer-section---------------->
                </body>
                </html>
                ';
        print $html;
    }

    private function htmlCagnotteVide(){
        $html = ' ';
        $cagnotte = 0;
        $contributeur = array();

        foreach ($this->liste as $list){
            $cagnotte = $cagnotte + $list->particip;
            $contributeur = $list->nom;
        }

        if($cagnotte == 0){
            $html = '
                    <div>
                       <h1>Votre cagnotte est vide mais vous pouvez la remplir !</h1> 
                    </div>
                    <div>
                    <h1><form action="./remplircagnotte" method="get">
                          
                                <input id="remplirCagnotte" type="submit" name=" " value="Remplir votre cagnotte"/>
                                
                                
                    </form></h1> 
                    </div>
                    <div>
                       <h1>Vos amis peuvent aussi contribuer ! Envoyez leur le lien de votre cagnotte !</h1> 
                    </div>
                    <div>
                    <h1><form action="./liencagnotte" method="get">
                                
                                <input id="lien cagnotte" type="submit" name=" " value="Obtenir un lien pour vos amis"/>
                    </form></h1> 
                    </div>
                    
            ';

        }
        elseif ($cagnotte > 0){
            $html = '
                <div>
                       <h1>La valeur de votre cagnotte est de '.$cagnotte.'€ !</h1> 
                    </div>
                    <div>
                    <h1> <form action="./remplircagnotte" method="get">
                                
                                <input id="RemplirCagnotte" type="submit" name=" " value="Remplir votre cagnotte"/>
                    </form></h1> 
                    </div>
                    <div>
                       <h1>Vos amis peuvent aussi contribuer ! Envoyez leur le lien de votre cagnotte !</h1> 
                    </div>
                    <div>
                    <h1> <form action="./liencagnotte" method="get">
                                
                                <input id="lien cagnotte" type="submit" name=" " value="Obtenir un lien pour vos amis"/>
                    </form></h1> 
                    </div>
            ';

        }


        return $html;
    }

    private function htmlRemplirCagnotte(){

        $arg = 0;

        $html ='<h1>Ajoutez de l\'argent à votre cagnotte !</h1>

                
                
                <h1><form action="./cagnotte" method="get">
                     <input type="text" maxlength="5" title="Valeur" name="subject" id="subject" format="NNNNN" value="'.$arg.'"/>   
                     <input type="submit" value="OK"/>
                </form></h1>


                <form method="post" class="credit-card" action="./valide">
                        <div class="form-header">
                            <h4 class="title">Données de la carte bancaire</h4>
                        </div>
                        <div class="form-body">

                            <!-- Card Number -->
                            <input type="text" class="card-number" placeholder="Numéro de carte">

                            <!-- Date Field -->
                            <div class="date-field">
                              <div class="month">
                                <select name="Month">
                                  <option value="01">01 - Janvier</option>
                                  <option value="02">02 - Février</option>
                                  <option value="03">03 - Mars</option>
                                  <option value="04">04 - Avril</option>
                                  <option value="05">05 - Mai</option>
                                  <option value="06">06 - Juin</option>
                                  <option value="07">07 - Juillet</option>
                                  <option value="08">08 - Août</option>
                                  <option value="09">09 - Septembre</option>
                                  <option value="10">10 - Octobre</option>
                                  <option value="11">11 - Novembre</option>
                                  <option value="12">12 - Décembre</option>
                                </select>
                              </div>
                              <div class="year">
                                <select name="Year">
                                  <option value="2017">2017</option>
                                  <option value="2018">2018</option>
                                  <option value="2019">2019</option>
                                  <option value="2020">2020</option>
                                  <option value="2021">2021</option>
                                  <option value="2022">2022</option>
                                  <option value="2023">2023</option>
                                  <option value="2024">2024</option>
                                  <option value="2025">2025</option>
                                </select>
                              </div>
                            </div>

                            <!-- Card Verification Field -->
                            <div class="card-verification">
                                <div class="cvv-input">
                                    <input type="text" placeholder="CVV">
                                </div>
                                <div class="cvv-details">
                                    <p>Code CVV se trouvant au dos de la carte</p>
                                </div>
                            </div>

                            <!-- Buttons -->
                            <!--<button type="submit" class="proceed-btn"><a href="#">Valider</a>
                            </button>-->
                        </div>
                    </form>';


        /**
        $Cagnotte = new Cagnotte();
        $Cagnotte->id = 1;
        $Cagnotte->nom = 'user';
        $Cagnotte->particip = $arg;
        $Cagnotte->save();
        */

        return $html;


    }


    private function htmlLienCagnotte(){

        $CagnotteC = new CagnotteController();
        $url = $CagnotteC::genererUrlCagnotte();

        $html = ' <div>
                        <h1>Lien pour vos amis : <a href="'.$url.'">ICI</a></h1>
                  </div>';
        return $html;
    }

}
