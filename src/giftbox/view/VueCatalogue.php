<?php

/**
 * Created by PhpStorm.
 * User: mathc
 * Date: 13/12/2016
 * Time: 08:16
 */
namespace giftbox\view;


class VueCatalogue
{
    protected $selecteur = -1;
    protected $liste = array();
    protected $listen = array();
    protected $swap="";

    function __construct($tab, $tabn = null)
    {
        $this->liste = $tab;
        $this->listen = $tabn;
    }


    public function render($selec)
    {
        $this->selecteur = $selec;
        $content = "";
        //$swap = "/giftbox";
        $swap = "./";
        switch ($this->selecteur) {
            case LIST_VIEW : {
                $content = $this->htmlListPrestation();
                $swap = "";
                break;
            }
            case PRESTA_VIEW : {
                $content = $this->htmlUnePrestation();
                $swap = "/..";
                break;
            }
            case LIST_CATEGORIES_VIEW : {
                $content = $this->htmlListCategories();
                $swap = "";
                break;
            }
            case LIST_PRESTATION_CATEGORIE_VIEW : {
                $swap = "/..";
                $this->swap = $swap;
                $content = $this->htmlListPrestationsCategorie();
                break;
            }

        }
        $html = '
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Giftbox</title>
                
                <!--<link rel="stylesheet" href="./web/css/bootstrap.css">-->
                <link href=".' . $swap . '/web/css/boot.css" rel="stylesheet" type="text/css" />
                <link href=".' . $swap . '/web/css/styles.css" rel="stylesheet" type="text/css" />
                
                <!--<link rel="stylesheet" href="./web/css/bootstrap-theme.css">-->
                <!--<link rel="stylesheet" href="./web/css/accueil.css">-->
                
                <link href=\'http://fonts.googleapis.com/css?family=Playfair+Display\' rel=\'stylesheet\' type=\'text/css\'>
                <link href=\'http://fonts.googleapis.com/css?family=Lato\' rel=\'stylesheet\' type=\'text/css\'>
                
                </head>
                <body>
                <div class="header-section">
                  <div class="header">
                    <div class="logo">
                      <a href=".' . $swap . '/"><h1>GiftBox</h1></a>
                    </div>
                    <div class="menu">
                      <ul>
                        <li><a href=".' . $swap . '/">Accueil</a></li>
                        <li><a href=".' . $swap . '/prestation">Catalogue</a></li>
                        <li><a href=".' . $swap . '/categorie">Catégories</a></li>
                        <li><a href=".' . $swap . '/coffret">Coffret</a></li>
                        <li><a href=".' . $swap . '/cagnotte">Cagnotte</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                    <div class="clear"></div>
                											
                											
                											    ' . $content . '
                											    
                											    
                	</div>				
                				<div class="clear"></div>
                <div class="footer-section">
                  <div class="footer">
                    <div class="panel marRight30">
                      <div class="title">
                        <h2>Liens utiles</h2>
                      </div>
                      <div class="content">
                        <ul>
                             <li><a href=".' . $swap . '/">Accueil</a></li>
                            <li><a href=".' . $swap . '/prestation">Catalogue</a></li>
                            <li><a href=".' . $swap . '/categorie">Catégories</a></li>
                            <li><a href=".' . $swap . '/coffret">Coffret</a></li>
                            <li><a href=".' . $swap . '/cagnotte">Cagnotte</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="panel marRight30">
                      <div class="title">
                        <h2>A propos</h2>
                      </div>
                      <div class="content">
                        <P><span>Bienvenur sur Giftbox</span></P>
                        <P>Nous vous présentons tout un catalogue de prestation</P>
                        <P>Vous pouvez créer vos coffret et les envoyez à vos amis</P>
                      </div>
                    </div>
                    <div class="panel">
                      <div class="title">
                        <h2>Copy Rights</h2>
                      </div>
                      <div class="content">
                        <div class="copyriight">
                          <P class="border-bottom">Giftbox <br />
                            Copy rights. 2017. All rights reserved.</P>
                          <P>Designed By : <a href="www.alltemplateneeds.com" target="_blank">www.alltemplateneeds.com</a><br />
                            Image courtesy.<a href="www.photorack.net" target="_blank"> www.photorack.net</a></P>
                        </div>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>
                </div>
                <!---------------end-footer-section---------------->
                </body>
                </html>
                ';
        print $html;
    }


    private function htmlListPrestation()
    {
        $html = '<section>
                    <h1>Nos articles :</h1>                     
                    <table style="width:100%">
                      <tr>
                        <!--<th><u>N°</u></th>-->
                        <th><u>Nom</u></th> 
                        <th><u>Catégorie</u></th> 
                       <!-- <th><u>Description</u></th>
                        <th><u>N° de catégorie</u></th>-->
                        <th><u>Image</u></th>
                        <th><u>
                        
                        Prix    
                        </u>
                        <a href="./croissant"> &#x25B2 <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a>
                        <a href ="./decroissant"> &#x25BC <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                        </th>   
                        
                        
                      </tr>
                    <!--</table>
                    <hr size="4" color="black" width=100%>-->
                    ';

        foreach ($this->liste as $list) {
            $html .= '<!--<table style="width:100%">-->
                    <tr>
                        <td>' . $list->nom . '</td>
                        <td>
                            <a href = "categorie/'.$list->categorie->id.'">
                                                        ' . $list->categorie->nom . '
                            </a>
                        </td>
                        <td>
                            <a href="prestation/' . $list->id . '">
                                <img src=.' . $this->swap . '/web/img/' . $list->img . ' alt="Image" style="width:304px;height:228px;">
                            </a>
                        </td>
                        <td>' . $list->prix . '€</td>    
                        <td>                            
                            <form action="./ajoutCoffret" method="post">
                                <input type="hidden" name="idPresta" value="' . $list->id . '" id="id"  />
                                <input id="ajouterPrestation" type="submit" name="' . $list->id . '" value="Ajouter la prestation au coffret"/>
                            </form>
                        </td>
                    </tr>
                    <!--</table>
                    <hr size="4" color="black" width=100%>-->';
        }

        $html .= '</table>
                    </section>';

        return $html;
    }


    private function htmlUnePrestation()
    {
        $it = 0;
        $html = "";
        $moy = 0;
        foreach ($this->liste as $list) {

            $html = '<section>
                    <h1>' . $list->nom . '</h1>
                    <table style="width:100%">
                      <tr>
                        <th>Nom</th> 
                        <th>Description</th>
                        <th>Catégorie</th>
                        <th>Image</th>
                        <th>Prix</th>
                        
                      </tr>';

            $html .= '<tr>
                        <td>' . $list->nom . '</td>
                        <td>' . $list->descr . '</td>
                        <td>
                            <a href = "categorie/'.$list->categorie->id.'">
                                                        ' . $list->categorie->nom . '
                            </a>
                        </td>
                        <td>
                            <img src=./../web/img/' . $list->img . ' alt="Image" style="width:304px;height:228px;">
                        </td>
                        <td>' . $list->prix . '€</td>
                        ';


            foreach($this->listen as $listn) {

                if($listn->id == $list->id) {
                    $moy = $moy + $listn->note;
                    $it++;

                }
            }



            if($it != 0){
                $html .=' <td >Note : '.$moy / $it.' / 5</td >';
            }

            $html .= '<td>                            
                            <form action="../ajoutCoffret" method="post">
                                <input type="hidden" name="idPresta" value="' . $list->id . '" id="id"  />
                                <input id="ajouterPrestation" type="submit" name="' . $list->id . '" value="Ajouter la prestation au coffret"/>
                            </form>
                      </td>';
        }





    $html .= '</tr></table>
                    </section>';
        return $html;
    }


    private function htmlListCategories()
    {
        $html = '<section>
                    <h1>Les catégories de prestation :</h1>                     
                    <table style="width:100%">
                      <tr>
                        <th><u>Nom</u></th> 
                      </tr>
                    <!--</table>
                    <hr size="4" color="black" width=100%>-->
                    ';

        foreach ($this->liste as $list) {
            $html .= '<!--<table style="width:100%">-->
                    <tr>
                        <td>
                            <a href="./categorie/'.$list->id.'">
                            ' . $list->nom . '   
                            </a>     
                        </td> 
                    </tr>
                    <!--<hr size="4" color="black" width=100%>-->';
        }
        $html .= '</table>
                    </section>';

        return $html;
    }


    private function htmlListPrestationsCategorie()
    {
        $html = '<section>
                    <h1>Nos articles de la catégorie '.$this->liste[1]->categorie->nom.' :</h1>
                    <table style="width:100%">
                      <tr>
                        <!--<th><u>N°</u></th>-->
                        <th><u>Nom</u></th> 
                       <!-- <th><u>Description</u></th>
                        <th><u>N° de catégorie</u></th>-->
                        <th><u>Image</u></th>
                        <th><u>
                        
                        Prix    
                        </u>
                        <a href="../croissant/'.$this->liste[1]->categorie->id.'"> &#x25B2 <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a>
                        <a href ="../decroissant/'.$this->liste[1]->categorie->id.'"> &#x25BC <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                        </th>   
                        
                        
                      </tr>
                    <!--</table>
                    <hr size="4" color="black" width=100%>-->
                    ';

        foreach ($this->liste as $list) {
            $html .= '<!--<table style="width:100%">-->
                    <tr>
                        <!--<td>' . $list->id . '</td>-->
                        <td>' . $list->nom . '</td>
                       <!-- <td>' . $list->descr . '</td>
                        <td>' . $list->cat_id . '</td>-->
                        <td>
                            <a href="../prestation/' . $list->id . '">
                                <img src=.' . $this->swap . '/web/img/' . $list->img . ' alt="Image" style="width:304px;height:228px;">
                            </a>
                        </td>
                        <td>' . $list->prix . '€</td>    
                        <td>                            
                            <form action="../ajoutCoffret" method="post">
                                <input type="hidden" name="idPresta" value="' . $list->id . '" id="id"  />
                                <input id="ajouterPrestation" type="submit" name="' . $list->id . '" value="Ajouter la prestation au coffret"/>
                            </form>
                        </td>
                    </tr>
                    <!--</table>
                    <hr size="4" color="black" width=100%>-->';
        }

        $html .= '</table>
                    </section>';

        return $html;
    }

}