<?php
/**
 * Created by PhpStorm.
 * User: mathc
 * Date: 19/12/2016
 * Time: 20:01
 */

namespace giftbox\view;


class VueAccueil
{
	protected $selecteur = -1;
	protected $liste = array();
    protected $listen = array();

	function __construct($tab, $tabn)
	{
		$this->liste = $tab;
        $this->listen = $tabn;
	}


	public function render($selec)
	{
		$this->selecteur = $selec;
		$content = $this->htmlUnePrestation();

		$html = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Giftbox</title>
                <link href="./web/css/styles.css" rel="stylesheet" type="text/css" />
                <link href="./web/css/boot.css" rel="stylesheet" type="text/css" />
                
                <!--<link rel="stylesheet" href="./web/css/bootstrap.css">-->
                <!--<link rel="stylesheet" href="./web/css/bootstrap-theme.css">-->
                <!--<link rel="stylesheet" href="./web/css/accueil.css">-->
                
                <link href=\'http://fonts.googleapis.com/css?family=Playfair+Display\' rel=\'stylesheet\' type=\'text/css\'>
                <link href=\'http://fonts.googleapis.com/css?family=Lato\' rel=\'stylesheet\' type=\'text/css\'>
            </head>
            <body>
            <div class="header-section">
              <div class="header">
                <div class="logo">
                  <a href="./"><h1>GiftBox</h1></a>
                </div>
                <div class="menu">
                  <ul>
                    <li><a href="./">Accueil</a></li>
                    <li><a href="./prestation">Catalogue</a></li>
                    <li><a href="./categorie">Catégories</a></li>
                    <li><a href="./coffret">Coffret</a></li>
                    <li><a href="./cagnotte">Cagnotte</a></li>
                  </ul>
                </div>
              </div>
            </div>
                <div class="clear"></div>
            											
            											
            											    '.$content.'
            											    
            											    
            	</div>				
            				<div class="clear"></div>
            <div class="footer-section">
              <div class="footer">
                <div class="panel marRight30">
                  <div class="title">
                    <h2>Liens utiles</h2>
                  </div>
                  <div class="content">
                    <ul>
                         <li><a href="./">Accueil</a></li>
                        <li><a href="./prestation">Catalogue</a></li>
                        <li><a href="./coffret">Coffret</a></li>
                        <li><a href="./cagnotte">Cagnotte</a></li>
                    </ul>
                  </div>
                </div>
                <div class="panel marRight30">
                  <div class="title">
                    <h2>A propos</h2>
                  </div>
                  <div class="content">
                    <P><span>Bienvenur sur Giftbox</span></P>
                    <P>Nous vous présentons tout un catalogue de prestation</P>
                    <P>Vous pouvez créer vos coffret et les envoyez à vos amis</P>
                  </div>
                </div>
                <div class="panel">
                  <div class="title">
                    <h2>Copy Rights</h2>
                  </div>
                  <div class="content">
                    <div class="copyriight">
                      <P class="border-bottom">Giftbox <br />
                        Copy rights. 2017. All rights reserved.</P>
                      <P>Designed By : <a href="www.alltemplateneeds.com" target="_blank">www.alltemplateneeds.com</a><br />
                        Image courtesy.<a href="www.photorack.net" target="_blank"> www.photorack.net</a></P>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <!---------------end-footer-section---------------->
            </body>
            </html>
                ';
		print $html;
	}


	private function htmlUnePrestation()
	{
        $html = '<section>
                    <h1>Nos meilleurs articles :</h1>                     
                    <table style="width:100%">
                      <tr>
                        <th><u>Nom</u></th> 
                        <th><u>Catégorie</u></th> 
                        <th>Description</th>
                        <th><u>Image</u></th>
                        <th><u>Prix</u></th>
                      </tr>
                    <!--</table>
                    <hr size="4" color="black" width=100%>-->
                    ';

        $best = array();
        $cat = array();

        foreach ($this->liste as $list){
            if(!in_array($list->categorie->id, $cat)){
                $cat[] = $list->categorie->id;

            }
        }

        for($i = 0, $size = count($cat)+1; $i < $size; $i++)
        {
            $best[$i] = 0;
        }


        foreach ($this->liste as $list) {
            $it = 0;
            $moy = 0;

            foreach ($this->listen as $listn) {

                if ($listn->id == $list->id) {
                    $moy = $moy + $listn->note;
                    $it++;

                }
            }

            if($it != 0){
                $moy = $moy / $it;
            }

            for($i = 1, $size = count($best)+1; $i<$size; $i++){


                if($list->categorie->id == $i){

                    if($moy > $best[$i]){

                        $best[$i] = $moy;

                    }
                }

            }

        }

        foreach ($this->liste as $list) {

            $it = 0;
            $moy = 0;

            foreach ($this->listen as $listn) {

                if ($listn->id == $list->id) {
                    $moy = $moy + $listn->note;
                    $it++;

                }
            }

            if($it != 0){
                $moy = $moy / $it;
            }

            for($i = 1, $size = count($best)+1; $i<$size; $i++){

                if($i == $list->categorie->id && $moy == $best[$i]) {

                    $html .= '<!--<table style="width:100%">-->
                    <tr>
                        
                        <td>' . $list->nom . '</td>
                         <td>
                            <a href = "categorie/' . $list->categorie->id . '">
                                                        ' . $list->categorie->nom . '
                            </a>
                        </td>
                        <td>' . $list->descr . '</td>
                        <td>
                            <a href="./prestation/' . $list->id . '">
                                <img src=./web/img/' . $list->img . ' alt="Image" style="width:304px;height:228px;">
                            </a>
                        </td>   
                        <td>' . $list->prix . '€</td> 
                           
                           <td>                            
                            <form action="./ajoutCoffret" method="post">
                                <input type="hidden" name="idPresta" value="' . $list->id . '" id="id"  />
                                <input id="ajouterPrestation" type="submit" name="' . $list->id . '" value="Ajouter la prestation au coffret"/>
                            </form>
                        </td>
                    ';


                    if ($it != 0) {
                        $html .= ' <td >Note : ' . $moy / $it . ' / 5</td >';
                    }

                    $best[$i] = 6;

                    $html .= '</tr>';
                }
            }
        }



        $html .= '</table></section>';

        return $html;
	}

}