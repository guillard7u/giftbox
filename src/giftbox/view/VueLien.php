<?php
/**
 * Created by PhpStorm.
 * User: mathc
 * Date: 18/01/2017
 * Time: 18:10
 */

namespace giftbox\view;


class VueLien
{
    protected $swap="";

    function __construct()
    {

    }

    public function render($url, $urlSuivi)
    {

        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Giftbox</title>
                
                <!--<link rel="stylesheet" href="./web/css/bootstrap.css">-->
                <link href=".' . $this->swap . '/web/css/boot.css" rel="stylesheet" type="text/css" />
                <link href=".' . $this->swap . '/web/css/styles.css" rel="stylesheet" type="text/css" />
                
                <!--<link rel="stylesheet" href="./web/css/bootstrap-theme.css">-->
                <!--<link rel="stylesheet" href="./web/css/accueil.css">-->
                
                <link href=\'http://fonts.googleapis.com/css?family=Playfair+Display\' rel=\'stylesheet\' type=\'text/css\'>
                <link href=\'http://fonts.googleapis.com/css?family=Lato\' rel=\'stylesheet\' type=\'text/css\'>
                
                </head>
                <body>
                <div class="header-section">
                  <div class="header">
                    <div class="logo">
                      <a href=".' . $this->swap . '/"><h1>GiftBox</h1></a>
                    </div>
                    <div class="menu">
                      <ul>
                        <li><a href=".' . $this->swap . '/">Accueil</a></li>
                        <li><a href=".' . $this->swap . '/prestation">Catalogue</a></li>
                        <li><a href=".' . $this->swap . '/categorie">Catégories</a></li>
                        <li><a href=".' . $this->swap . '/coffret">Coffret</a></li>
                        <li><a href=".' . $this->swap . '/cagnotte">Cagnotte</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                    <div class="clear"></div>
                											
                		<h1>Voici le lien pour distribuer le coffret : <a href="'.$url.'">'.$url.'</a></h1>	
                										
                		<h1>Voici votre url pour suivre le coffret : <a href="'.$urlSuivi.'">'.$urlSuivi.'</a></h1>
 									    
                	</div>				
                				<div class="clear"></div>
                <div class="footer-section">
                  <div class="footer">
                    <div class="panel marRight30">
                      <div class="title">
                        <h2>Liens utiles</h2>
                      </div>
                      <div class="content">
                        <ul>
                             <li><a href=".' .$this->swap . '/">Accueil</a></li>
                            <li><a href=".' . $this->swap . '/prestation">Catalogue</a></li>
                            <li><a href=".' . $this->swap . '/categorie">Catégories</a></li>
                            <li><a href=".' . $this->swap . '/coffret">Coffret</a></li>
                            <li><a href=".' . $this->swap . '/cagnotte">Cagnotte</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="panel marRight30">
                      <div class="title">
                        <h2>A propos</h2>
                      </div>
                      <div class="content">
                        <P><span>Bienvenur sur Giftbox</span></P>
                        <P>Nous vous présentons tout un catalogue de prestation</P>
                        <P>Vous pouvez créer vos coffret et les envoyez à vos amis</P>
                      </div>
                    </div>
                    <div class="panel">
                      <div class="title">
                        <h2>Copy Rights</h2>
                      </div>
                      <div class="content">
                        <div class="copyriight">
                          <P class="border-bottom">Giftbox <br />
                            Copy rights. 2017. All rights reserved.</P>
                          <P>Designed By : <a href="www.alltemplateneeds.com" target="_blank">www.alltemplateneeds.com</a><br />
                            Image courtesy.<a href="www.photorack.net" target="_blank"> www.photorack.net</a></P>
                        </div>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>
                </div>
                <!---------------end-footer-section---------------->
                </body>
                </html>
        
        ';


        print $html;
    }


}