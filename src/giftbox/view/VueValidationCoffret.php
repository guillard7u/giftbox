<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 31/12/2016
 * Time: 14:27
 */

namespace giftbox\view;
define("FORMULAIRE_VIEW", 1);
define("RECAPITULATIF_VIEW", 2);




class VueValidationCoffret
{
    protected $selecteur = -1;
    protected $nom;
    protected $mail;
    protected $prenom;
    protected $paiement;
    protected $commentaire;
    protected $erreurs = array();
    protected $liste;
    protected $quantite;

//    protected $nameErr = $emailErr = $genderErr = $websiteErr = "";

    function __construct($n = "", $pn = "", $m = "", $c = "", $p = "",
                         $erreurs = null, $prestations = null, $quantite = null)
    {
        $this->nom = $n;
        $this->prenom = $pn;
        $this->mail = $m;
        $this->commentaire = $c;
        $this->paiement = $p;
        $this->erreurs = $erreurs;
        $this->liste = $prestations;
        $this->quantite = $quantite;

        if (empty($this->erreurs)) {
            $this->erreurs['nom'] = "";
            $this->erreurs['prenom'] = "";
            $this->erreurs['mail'] = "";
            $this->erreurs['paiement'] = "";
        }
        if(empty($this->erreurs['paiement'])){
            $this->erreurs['paiement'] = "";
        }

    }


    public function render($selec = 1)
    {
        $this->selecteur = $selec;

        // TODO trouver le moyen de le passer dans le controller
//        if (empty($this->erreurs)) {
//            $this->selecteur = RECAPITULATIF_VIEW;
//        }

        $content = "";
        $swap = '/giftbox';
        switch ($this->selecteur) {
            case FORMULAIRE_VIEW: {
                $content = $this->htmlCoordonnees();
                $swap = "";
                break;
            }
            case RECAPITULATIF_VIEW: {
                $content = $this->htmlRecapitulatif();
                $swap = "";
                break;
            }
        }

        $html = '
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Giftbox</title>
            <link href="./web/css/styles.css" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" href=".' . $swap . '/web/css/boot.css">
             
             
            <!--<link rel="stylesheet" href="./web/css/bootstrap.css">
            <link rel="stylesheet" href="./web/css/bootstrap-theme.css">-->
           
            
            <link href=\'http://fonts.googleapis.com/css?family=Playfair+Display\' rel=\'stylesheet\' type=\'text/css\'>
            <link href=\'http://fonts.googleapis.com/css?family=Lato\' rel=\'stylesheet\' type=\'text/css\'>
        </head>
        <body>
        <div class="header-section">
          <div class="header">
            <div class="logo">
              <a href="./"><h1>GiftBox</h1></a>
            </div>
            <div class="menu">
              <ul>
                <li><a href="./">Accueil</a></li>
                <li><a href="./prestation">Catalogue</a></li>
                <li><a href="./coffret">Coffret</a></li>
                <li><a href="./cagnotte">Cagnotte</a></li>
              </ul>
            </div>
          </div>
        </div>
            <div class="clear"></div>
        											
        											
        											    '.$content.'
        											    
        											    
        	</div>				
        				<div class="clear"></div>
        <div class="footer-section">
          <div class="footer">
            <div class="panel marRight30">
              <div class="title">
                <h2>Liens utiles</h2>
              </div>
              <div class="content">
                <ul>
                     <li><a href="./">Accueil</a></li>
                    <li><a href="./prestation">Catalogue</a></li>
                    <li><a href="./coffret">Coffret</a></li>
                    <li><a href="./cagnotte">Cagnotte</a></li>
                </ul>
              </div>
            </div>
            <div class="panel marRight30">
              <div class="title">
                <h2>A propos</h2>
              </div>
              <div class="content">
                <P><span>Bienvenur sur Giftbox</span></P>
                <P>Nous vous présentons tout un catalogue de prestation</P>
                <P>Vous pouvez créer vos coffret et les envoyez à vos amis</P>
              </div>
            </div>
            <div class="panel">
              <div class="title">
                <h2>Copy Rights</h2>
              </div>
              <div class="content">
                <div class="copyriight">
                  <P class="border-bottom">Giftbox <br />
                    Copy rights. 2017. All rights reserved.</P>
                  <P>Designed By : <a href="www.alltemplateneeds.com" target="_blank">www.alltemplateneeds.com</a><br />
                    Image courtesy.<a href="www.photorack.net" target="_blank"> www.photorack.net</a></P>
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <!---------------end-footer-section---------------->
        </body>
        </html>
                ';
        print $html;
    }


    // TODO filter_var($_POST['query'], FILTER_SANITIZE_STRING); ??
    private function htmlCoordonnees()
    {
        $html = '<section align="center"> 
            <br>
            <form method="post" action="./validationCoffret"> ';


        $html .= 'Nom : <input type="text" name="nom" value="' . $this->nom . '">';
        if (!empty($this->erreurs['nom'])) {
            $html .= '<span class="erreur"> ' . $this->erreurs['nom'] . '</span>';
        }

        $html .= '<br><br>
                Prénom : <input type="text" name="prenom" value="' . $this->prenom . '">';
        if (!empty($this->erreurs['prenom'])) {
            $html .= '<span class="erreur"> ' . $this->erreurs['prenom'] . '</span>';
        }

        $html .= '<br><br>
                Mail : <input type="text" name="mail" value="' . $this->mail . '">';
        if (!empty($this->erreurs['mail'])) {
            $html .= '<span class="erreur"> ' . $this->erreurs['mail'] . '</span>';
        }
        $html .= '<br><br>
                Message pour le destinataire : <textarea name="commentaire" rows="5" cols="40">' . $this->commentaire . '</textarea>
                <br><br>
                Mode de paiement :';
        if ($this->paiement == "classique") {
            $html .= '<input type = "radio" name = "paiement" value = "classique" checked="checked"> Classique
                     <input type = "radio" name = "paiement" value = "cagnotte" > Cagnotte
                     <br ><br >
                     <input type = "submit" name = "valider" value = "Valider" >
            </form>
               </section>';
        } elseif ($this->paiement == "cagnotte") {
            $html .= '<input type = "radio" name = "paiement" value = "classique" > Classique
                     <input type = "radio" name = "paiement" value = "cagnotte" checked="checked"> Cagnotte
                     <br ><br >
                     <input type = "submit" name = "valider" value = "Valider" >
            </form >
               </section>';
        } else {
            $html .= '<input type = "radio" name = "paiement" value = "classique" > Classique
                      <input type = "radio" name = "paiement" value = "cagnotte" > Cagnotte
                      <span class="erreur">   ' . $this->erreurs['paiement'] . '</span>
                      <br><br>
                     <input type = "submit" name = "valider" value = "Valider" >
            </form>
               </section>';
        }
        return $html;
    }


    private function htmlRecapitulatif()
    {
        $html = '<section>
                    <h1>Contenu du coffret</h1>
                    <table style="width:100%">
                      <tr>
                        <th>Id</th>
                        <th>Nom</th> 
                        <th>Description</th>
                        <th>Id catégorie</th>
                        <th>Image</th>
                        <th>Quantité</th>
                        <th>Prix</th>
                      </tr>';
        $i = 0;
        $prixTot = 0;
        foreach ($this->liste as $list) {
            $html .= '<tr>
                        <td>' . $list->id . '</td>
                        <td>' . $list->nom . '</td>
                        <td>' . $list->descr . '</td>
                        <td>' . $list->cat_id . '</td>
                        <td>
                            <a href="prestation/' . $list->id . '">
                                <img src=./web/img/' . $list->img . ' alt="Image" style="width:304px;height:228px;">
                            </a>
                        </td>
                        <td>
                            ' . $list->id . '
                        </td>
                        <td>' . $list->prix . '€</td>
                        
                        
                        
                    </tr>';
            $prixTot += $this->quantite[$i] * $list->prix;
            $i++;
        }


        $html .= '    </table>
                  <br>
                  <p align="right">Prix du coffret : ' . $prixTot . '€</p>
                  <div aligne="right">
                    Vous avez sélectionné le mode de paiement ' . $this->paiement . '.
                  </div>
                  
                  </section>';

        $html .= '<div align="center">
                  	<form method="post" action = "./payement">
                  		<input type="submit" value="Payer"/>
                  	</form>
                  </div>';

        return $html;
    }
}