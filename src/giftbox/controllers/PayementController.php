<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 10/01/2017
 * Time: 11:43
 */

namespace giftbox\controllers;


define("FORMULAIRE_VIEW", 1);

use giftbox\view\VuePayement;
use /** @noinspection PhpUndefinedNamespaceInspection */
    Illuminate\Database\Capsule\Manager as DB;

// validationCoffret.php
if(!isset($_SESSION))
{
    session_start();
}




class PayementController
{


    public static function afficherFormulaireCB()
    {
        $vue = new VuePayement();
        $vue->render(FORMULAIRE_VIEW);

    }
}