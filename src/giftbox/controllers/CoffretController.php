<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 16/12/2016
 * Time: 09:43
 */

namespace giftbox\controllers;

define("COFFRET_NULL_VIEW", 1);
define("COFFRET_VIEW", 2);
define("COFFRET_CATE_VIEW", 3);
define("COFFRET_OFFERT_VIEW", 4);
define("COFFRET_CHOIX_VIEW", 5);
define("COFFRET_ALL_VIEW", 6);
define("COFFRET_ONE_VIEW", 7);
define("COFFRET_SUIVI_VIEW", 8);

/*use giftbox\models\Prestation;
use giftbox\models\Coffret;
use giftbox\models\AppartientCoffret;*/
use giftbox\models\Coffret;
use giftbox\models\PrestaCoffret;
use giftbox\models\Prestation;
use giftbox\view\VueCatalogue;
use giftbox\view\VueCoffret;
use /** @noinspection PhpUndefinedNamespaceInspection */
    Illuminate\Database\Capsule\Manager as DB;

// validationCoffret.php
if (!isset($_SESSION)) {
    session_start();
}




class CoffretController
{


    public static function ajouterPrestation($idPresta)
    {
//        if (!isset($_SESSION['idCoffret'])) {
//            $_SESSION['idCoffret'] = Coffret::insertGetId(['prix' => '0.0']);
//        }

        if (!isset($_SESSION['panier'])) {
            $_SESSION['panier'] = array();
        }

        // TODO Faire les affichages


//        $where = ['coffret_id' => $_SESSION['idCoffret'], 'presta_id' => $idPresta];
//        if (PrestaCoffret::where($where)->count() > 0) {
//            $p = PrestaCoffret::where($where)->first();
//            $p->quantite=$p->quantite + 1;
//            $p->save();
//            $prestation = Prestation::where('id', '=', $idPresta)->first()   ;
//            $coffret = Coffret::where('id', '=', $_SESSION['idCoffret'])->first();
//            print $coffret->prix + $prestation->prix;
//            $coffret->prix =$coffret->prix + $prestation->prix;
//            $coffret->save();
//
//        } else {
//            PrestaCoffret::insert(['coffret_id' => $_SESSION['idCoffret'], 'presta_id' => $idPresta]);
//            $prestation = Prestation::where('id', '=', $idPresta)->first()   ;
//            $coffret = Coffret::where('id', '=', $_SESSION['idCoffret'])->first();
////            $coffret->prix =$coffret->prix + $prestation->prix;
//            $coffret->update(['prix' => $coffret->prix + $prestation->prix]);
////            $coffret->save();
//        }

        if (isset($_SESSION['panier'][$idPresta])) {
            $_SESSION['panier'][$idPresta] += 1;
        } else {
            $_SESSION['panier'][$idPresta] = 1;
        }

    }

    public static function supprimerPrestation($idPresta)
    {
//        if (isset($_SESSION['idCoffret'])) {
//            $where = ['coffret_id' => $_SESSION['idCoffret'], 'presta_id' => $idPresta];
//            $p = PrestaCoffret::where($where)->first();
//            $coffret = Coffret::where('id', '=', $_SESSION['idCoffret'])->first();
//            $prestation = Prestation::where('id', '=', $idPresta)->first();
//            $coffret->prix -= $prestation->prix * $p->quantite;
//            $coffret->save();
//            $p->delete();
//        }

        if (isset($_SESSION['panier'][$idPresta])) {
            $_SESSION['panier'][$idPresta] -= 1;
            if ($_SESSION['panier'][$idPresta] <= 0) {
                unset($_SESSION['panier'][$idPresta]);
            }
        }
    }

    public static function supprimerPrestationTotale($idPresta)
    {
        if (isset($_SESSION['panier'][$idPresta])) {
            unset($_SESSION['panier'][$idPresta]);
        }
    }


    public static function changerNbPrestation($idPresta, $newQuantity)
    {
//        $where = ['coffret_id' => $_SESSION['idCoffret'], 'presta_id' => $idPresta];
//        if (PrestaCoffret::where($where)->count() > 0) {
//            $where = ['coffret_id' => $_SESSION['idCoffret'], 'presta_id' => $idPresta];
//            $p = PrestaCoffret::where($where)->first();
//            if ($newQuantity == 0) {
//                $p->delete();
//            } else {
////                $p->update(['quantite' => $newQuantity]);
//                $p->quantite = $newQuantity;
//                $p->save();
//            }
//            $prestaCoffret = PrestaCoffret::where(['coffret_id' => $_SESSION['idCoffret']])->get();
//            $coffret = Coffret::where('id', '=', $_SESSION['idCoffret'])->first();
//            $coffret->prix = 0;
//            $prixTot=0;
//            foreach ($prestaCoffret as $pc)
//            {
//                $prestation = Prestation::where('id', '=', $pc->presta_id)->first();
//                $prixTot = $prixTot+ ($pc->quantite * $prestation->prix);
//            }
//            $coffret->prix = $prixTot;
////            $coffret->update(['prix' => $prixTot]);
//            $coffret->save();
//
//        }

        if (isset($_SESSION['panier'][$idPresta])) {
            $_SESSION['panier'][$idPresta] = $newQuantity;
            if ($_SESSION['panier'][$idPresta] <= 0) {
                unset($_SESSION['panier'][$idPresta]);
            }
        }
    }

    public static function payementMauvaisNbCat()
    {
//		if (!isset($_SESSION['idCoffret'])) {
//			$vue = new VueCoffret();
//			$vue->render(COFFRET_NULL_VIEW);
//		} elseif (PrestaCoffret::where(['coffret_id' => $_SESSION['idCoffret']])->count() == 0) {
//			$vue = new VueCoffret();
//			$vue->render(COFFRET_NULL_VIEW);
//		} else {
//			$prestationCoffret = PrestaCoffret::where('coffret_id', '=', $_SESSION['idCoffret'])->get();  // TODO Changer le nom de la variable ?
//
//			$prestations = array();
//			$quantite = array();
//			foreach ($prestationCoffret as $pc) {
//				$p = \giftbox\models\Prestation::where('id', '=', $pc->presta_id)->first();
//				$quantite[] = $pc->quantite;
//				$prestations[] = $p;
//			}
//
//
//			$vue = new VueCoffret($prestations, $quantite);
//			$vue->render(COFFRET_CATE_VIEW);
//		}


        if (!isset($_SESSION['panier']) or count($_SESSION['panier']) == 0) {
            $vue = new VueCoffret();
            $vue->render(COFFRET_NULL_VIEW);
        } else {

            $prestations = array();
            $quantite = array();
            foreach ($_SESSION['panier'] as $value => $key) {
                $p = \giftbox\models\Prestation::where('id', '=', $value)->first();
                $quantite[] = $key;
                $prestations[] = $p;
            }


            $vue = new VueCoffret($prestations, $quantite);
            $vue->render(COFFRET_CATE_VIEW);
        }
    }


    public static function afficherCoffret()
    {
//        if (!isset($_SESSION['idCoffret'])) {
//            $vue = new VueCoffret();
//            $vue->render(COFFRET_NULL_VIEW);
//        } elseif (PrestaCoffret::where(['coffret_id' => $_SESSION['idCoffret']])->count() == 0) {
//            $vue = new VueCoffret();
//            $vue->render(COFFRET_NULL_VIEW);
//        } else {
//            $prestationCoffret = PrestaCoffret::where('coffret_id', '=', $_SESSION['idCoffret'])->get();  // TODO Changer le nom de la variable ?
//
//            $prestations = array();
//            $quantite = array();
//            foreach ($prestationCoffret as $pc) {
//                $p = \giftbox\models\Prestation::where('id', '=', $pc->presta_id)->first();
//                $quantite[] = $pc->quantite;
//                $prestations[] = $p;
//            }
//
//
//            $vue = new VueCoffret($prestations, $quantite);
//            $vue->render(COFFRET_VIEW);
//
//
//            /*foreach ($prestations as $prestation) {
//                $temp = $prestation->presta_id;
//                $p = \giftbox\models\Prestation::where('id', '=', $temp)->first();
//                print $p.'</br>'.'</br>';
//            }*/
//        }

        if (!isset($_SESSION['panier'])) {
            $vue = new VueCoffret();
            $vue->render(COFFRET_NULL_VIEW);
        } elseif (count($_SESSION['panier']) == 0) {
            $vue = new VueCoffret();
            $vue->render(COFFRET_NULL_VIEW);
        } else {


            $prestations = array();
            $quantite = array();
            foreach ($_SESSION['panier'] as $value => $key) {
                $p = \giftbox\models\Prestation::where('id', '=', $value)->first();
                $quantite[] = $key;
                $prestations[] = $p;
            }


            $vue = new VueCoffret($prestations, $quantite);
            $vue->render(COFFRET_VIEW);


            /*foreach ($prestations as $prestation) {
                $temp = $prestation->presta_id;
                $p = \giftbox\models\Prestation::where('id', '=', $temp)->first();
                print $p.'</br>'.'</br>';
            }*/
        }
    }

    public static function afficherCoffretChoix()
    {
//        if (!isset($_SESSION['idCoffret'])) {
//            $vue = new VueCoffret();
//            $vue->render(COFFRET_NULL_VIEW);
//        } elseif (PrestaCoffret::where(['coffret_id' => $_SESSION['idCoffret']])->count() == 0) {
//            $vue = new VueCoffret();
//            $vue->render(COFFRET_NULL_VIEW);
//        } else {
//            $prestationCoffret = PrestaCoffret::where('coffret_id', '=', $_SESSION['idCoffret'])->get();  // TODO Changer le nom de la variable ?
//
//            $prestations = array();
//            $quantite = array();
//            foreach ($prestationCoffret as $pc) {
//                $p = \giftbox\models\Prestation::where('id', '=', $pc->presta_id)->first();
//                $quantite[] = $pc->quantite;
//                $prestations[] = $p;
//            }
//
//
//            $vue = new VueCoffret($prestations, $quantite);
//            $vue->render(COFFRET_VIEW);
//
//
//            /*foreach ($prestations as $prestation) {
//                $temp = $prestation->presta_id;
//                $p = \giftbox\models\Prestation::where('id', '=', $temp)->first();
//                print $p.'</br>'.'</br>';
//            }*/
//        }

//        if (!isset($_SESSION['panier'])) {
//            $vue = new VueCoffret();
//            $vue->render(COFFRET_NULL_VIEW);
//        } elseif (count($_SESSION['panier']) == 0) {
//            $vue = new VueCoffret();
//            $vue->render(COFFRET_NULL_VIEW);
//        } else {


            $prestations = array();
            $quantite = array();
            foreach ($_SESSION['panier'] as $value => $key) {
                $p = \giftbox\models\Prestation::where('id', '=', $value)->first();
                $quantite[] = $key;
                $prestations[] = $p;
            }


            $vue = new VueCoffret($prestations, $quantite);
            $vue->render(COFFRET_CHOIX_VIEW);


            /*foreach ($prestations as $prestation) {
                $temp = $prestation->presta_id;
                $p = \giftbox\models\Prestation::where('id', '=', $temp)->first();
                print $p.'</br>'.'</br>';
            }*/
//        }
    }

    public static function afficherCoffretAll($id)
    {
        $coffret = Coffret::where(['id' => $id])->first();
        $coffret->etat = 'Ouvert All';
        $coffret->save();
        $prestationCoffret = PrestaCoffret::where('coffret_id', '=', $id)->get();  // TODO Changer le nom de la variable ?

        $prestations = array();
        $quantite = array();
        foreach ($prestationCoffret as $pc) {
            $p = \giftbox\models\Prestation::where('id', '=', $pc->presta_id)->first();
            $quantite[] = $pc->quantite;
            $prestations[] = $p;
        }


        $vue = new VueCoffret($prestations, $quantite, $id);
        $vue->render(COFFRET_ALL_VIEW);


        /*foreach ($prestations as $prestation) {
            $temp = $prestation->presta_id;
            $p = \giftbox\models\Prestation::where('id', '=', $temp)->first();
            print $p.'</br>'.'</br>';
        }*/

    }

    public static function afficherCoffretOne($id)
    {
        if(!isset($_SESSION['prestationsCoffretOffert']))
        {
            $coffret = Coffret::where(['id' => $id])->first();
            $coffret->etat = 'Ouverture en cours';
            $coffret->save();
            $prestationCoffret = PrestaCoffret::where('coffret_id', '=', $id)->get();  // TODO Changer le nom de la variable ?

            $_SESSION['prestationsCoffretOffert'] = array();
            $quantite = array();
            foreach ($prestationCoffret as $pc) {
                $p = \giftbox\models\Prestation::where('id', '=', $pc->presta_id)->first();
                $quantite[] = $pc->quantite;
                $_SESSION['prestationsCoffretOffert'][] = $p;
            }


            $vue = new VueCoffret($_SESSION['prestationsCoffretOffert'], null, $id);
            $vue->render(COFFRET_ONE_VIEW);
        }
        else
        {
            if(empty($_SESSION['prestationsCoffretOffert']))
            {
                CoffretController::afficherCoffretAll($id);
            }
            else {
                $vue = new VueCoffret($_SESSION['prestationsCoffretOffert'], null, $id);
                $vue->render(COFFRET_ONE_VIEW);
            }
        }




    }

    public static function afficherCoffretOffert($hash)
    {
        $coffrets = Coffret::get();
        $message = "";
        $prestations = array();
        foreach ($coffrets as $coffret) {

            if (password_verify($coffret->id, $hash)) {
                $prestaCoffret = $coffret->prestaCoffret;

                $message = $coffret->message;
                foreach ($prestaCoffret as $pc) {
                    $prestations[] = Prestation::where(['id' => $pc->presta_id])->get();
                }
                $vue = new VueCoffret($prestations, null, $coffret->id);
                $vue->render(COFFRET_OFFERT_VIEW);
            }
        }


    }

    public static function afficherSuivi($hash)
    {
        $coffrets = Coffret::get();
        $message = "";
        foreach ($coffrets as $coffret) {

            if (password_verify('gestion'.$coffret->id, $hash)) {
                $vue = new VueCoffret(null, null, $coffret->id);
                $vue->render(COFFRET_SUIVI_VIEW);
            }
        }


    }


    public static function validerCoffret()
    {
        if (isset($_SESSION['panier'])) {
            if (count($_SESSION['panier']) != 0) {
                $_SESSION['idCoffret'] = Coffret::insertGetId(['prix' => '0.0']);
                $prixTot = 0;

                foreach ($_SESSION['panier'] as $key => $value) {
                    PrestaCoffret::insert(['coffret_id' => $_SESSION['idCoffret'], 'presta_id' => $key, 'quantite' => $value]);
                    $prestation = Prestation::where('id', '=', $key)->first();
                    $prixTot += $prestation->prix * $value;
                }

                $coffret = Coffret::where('id', '=', $_SESSION['idCoffret'])->first();
                $coffret->update(['prix' => $prixTot]);
                $coffret->update(['message' => $_SESSION['commentaire']]);
                $coffret->update(['nom' => $_SESSION['nom']]);
                $coffret->update(['mail' => $_SESSION['mail']]);
                $coffret->update(['prenom' => $_SESSION['prenom']]);
                $coffret->update(['payement' => 1]);
                $coffret->update(['etat' => 'En cours de transmission']);
                $coffret->save();
                unset($_SESSION['panier']);
//                unset($_SESSION['commentaire']);
//                unset($_SESSION['nom']);
//                unset($_SESSION['prenom']);

            }
        }
    }
}



