<?php
/**
 * Created by PhpStorm.
 * User: mathc
 * Date: 18/01/2017
 * Time: 14:40
 */


namespace giftbox\controllers;

/*use giftbox\models\Prestation;
use giftbox\models\Coffret;
use giftbox\models\AppartientCoffret;*/
use giftbox\models\Cagnotte;
use giftbox\models\Coffret;
use giftbox\models\PrestaCoffret;
use giftbox\models\Prestation;
use giftbox\view\VueCagnotte;
use giftbox\view\VueCatalogue;
use giftbox\view\VueCoffret;
use /** @noinspection PhpUndefinedNamespaceInspection */
    Illuminate\Database\Capsule\Manager as DB;


define("CAGNOTTE_VIDE", 1);
define("REMPLIR_CAGNOTTE", 2);
define("LIEN_CAGNOTTE", 3);

// validationCoffret.php
if (!isset($_SESSION)) {
    session_start();
}



class CagnotteController
{

    public static function afficherCagnotte()
    {
        $cagnotte = Cagnotte::get();
        $vue = new VueCagnotte($cagnotte);
        $vue->render(1);
    }


    public static function remplirCagnotte()
    {
        $cagnotte = Cagnotte::get();
        $vue = new VueCagnotte($cagnotte);
        $vue->render(2);
    }

    public static function lienCagnotte()
    {
        $cagnotte = Cagnotte::get();
        $vue = new VueCagnotte($cagnotte);
        $vue->render(3);
    }

    public static function genererUrlCagnotte()
    {

            /**$hash = password_hash(1, PASSWORD_DEFAULT, 1);
            var_dump(password_verify(1, $hash));
            while (strpos ( $hash, '/') != false)
            {
                $hash = password_hash(1, PASSWORD_DEFAULT, 1);
            }*/
            //\giftbox\models\Coffret::where('id', '=', $_SESSION['idCoffret'])->first()->update(['url' => $hash]); // TODO verifier, non nécessaire?
            $url = 'https://webetu.iutnc.univ-lorraine.fr/www/guillard7u/PHP/giftbox/cagnotte/remplircagnotte';
           return $url;

    }

}