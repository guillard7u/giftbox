<?php
/**
 * Created by PhpStorm.
 * User: mathc
 * Date: 19/12/2016
 * Time: 19:58
 */

namespace giftbox\controllers;

use giftbox\models\Prestation;
use giftbox\view\VueAccueil;
use giftbox\models\Notes;




class AccueilController
{
    public static function afficherPageAccueil()
    {
        $prestations = Prestation::get();
        $notes = Notes::get();

        $vue = new VueAccueil($prestations, $notes);
        $vue->render(1);
    }
}