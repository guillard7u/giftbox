<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 18/01/2017
 * Time: 22:36
 */

namespace giftbox\controllers;
use Illuminate\Database\Capsule\Manager as DB;

class ConnectionController
{
    public static function connection()
    {
        $iniCo = parse_ini_file('src/conf/conf.ini');

        $db = new DB();
        $db->addConnection([
            'driver' => $iniCo['driver'],
            'host' => $iniCo['host'],
            'database' => $iniCo['database'],
            'username' => $iniCo['username'],
            'password' => $iniCo['password'],
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => ''
        ]);

        $db->setAsGlobal();
        $db->bootEloquent();
    }
}