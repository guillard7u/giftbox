<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 12/12/2016
 * Time: 11:41
 */

namespace giftbox\controllers;
use giftbox\models\Prestation;
use giftbox\models\Notes;
use giftbox\models\Categorie;
use giftbox\view\VueCatalogue;






class CatalogueController
{
    public static function afficherListePrestations()
    {
        $prestations = Prestation::get();
        $notes = Notes::get();
        $vue = new VueCatalogue($prestations, $notes);
        $vue->render(LIST_VIEW);
    }

    public static function afficherUnePrestation($id)
    {
        $prestations = Prestation::select('id', 'nom', 'descr', 'cat_id', 'img', 'prix')->where('id', '=', $id)->get();
        $notes = Notes::select('id', 'note')->get();
        $vue = new VueCatalogue($prestations, $notes);
        $vue->render(PRESTA_VIEW);
        /*
        foreach ($prestations as $prestation) {
            //print $prestation->id . ' ' . $prestation->nom . ' ' . $prestation->descr . ' ' . $prestation->cat_id . ' ' . $prestation->img . ' ' . $prestation->prix.'</br></br>';
            $vue-> // TODO
        }
        */
    }

    public static function afficherListePrixCroissant()
    {
        $prestations = Prestation::orderBy('prix')->get();
        $notes = Notes::select('id', 'note')->get();
        $vue = new VueCatalogue($prestations, $notes);
        $vue->render(LIST_VIEW);
    }

    public static function afficherListePrixDecroissant()
    {
        $prestations = Prestation::orderBy('prix', 'desc')->get();
        $notes = Notes::select('id', 'note')->get();
        $vue = new VueCatalogue($prestations, $notes);
        $vue->render(LIST_VIEW);
    }

    public static function afficherListePrixCroissantCategorie($id)
    {
        $prestations = Prestation::where(['cat_id' => $id])->orderBy('prix')->get();
        $notes = Notes::select('id', 'note')->get();
        $vue = new VueCatalogue($prestations, $notes);
        $vue->render(LIST_PRESTATION_CATEGORIE_VIEW);
    }

    public static function afficherListePrixDecroissantCategorie($id)
    {
        $prestations = Prestation::where(['cat_id' => $id])->orderBy('prix', 'desc')->get();
        $notes = Notes::select('id', 'note')->get();
        $vue = new VueCatalogue($prestations, $notes);
        $vue->render(LIST_PRESTATION_CATEGORIE_VIEW);
    }

    public static function afficherCategories()
    {
        $categories = Categorie::select('id', 'nom')->get();
        $notes = Notes::select('id', 'note')->get();
        $vue = new VueCatalogue($categories, $notes);
        $vue->render(LIST_CATEGORIES_VIEW);
    }

    public static function afficherContenuCategories($id)
    {


//        $prestations = Prestation::select('id', 'nom', 'descr', 'cat_id', 'img', 'prix')->get();
//        $categories = Categorie::select('id', 'nom')->get();
//
//
//        foreach ($prestations as $prestation) {
//            $p = $prestation->categorie ;
//            print $prestation->id . ' ' . $prestation->nom . ' ' . $prestation->descr . ' ' . $prestation->cat_id . ' ' . $prestation->img . ' ' . $prestation->prix.'    ';
//            print '</br>'.$p->nom.'</br></br>';
//        }
//        print '</br>';
//        print '</br>';
//
//        foreach ($categories as $categorie)
//        {
//            $c = $categorie->prestations;
//            print $categorie->id.' '.$categorie->nom.'</br>';
//
//            foreach ($c as $c1)
//            {
//                print '.    '.$c1->nom.'</br>';
//            }
//            print '</br>';
//
//        }

        $categorie = Categorie::where(['id' => $id])->first();
        $prestations = $categorie->prestations;
        $vue = new VueCatalogue($prestations);
        $vue->render(LIST_PRESTATION_CATEGORIE_VIEW);
    }
}