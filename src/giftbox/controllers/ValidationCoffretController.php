<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 31/12/2016
 * Time: 14:26
 */

namespace giftbox\controllers;


/*use giftbox\models\Prestation;
use giftbox\models\Coffret;
use giftbox\models\AppartientCoffret;*/
use giftbox\models\Coffret;
use giftbox\models\PrestaCoffret;
use giftbox\view\VueCatalogue;
use giftbox\view\VueCoffret;
use giftbox\view\VueValidationCoffret;
use /** @noinspection PhpUndefinedNamespaceInspection */
    Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';




class ValidationCoffretController
{
    public static function afficher()
    {
        if (!isset($_SESSION)) {
            session_start();
        }

        if (!isset($_SESSION['panier'])) {
            \giftbox\controllers\CoffretController::afficherCoffret();
        } else {

            if (count($_SESSION['panier']) != 0) {
                foreach ($_SESSION['panier'] as $idPresta => $value) {
                    //$prestaCoffret = \giftbox\models\PrestaCoffret::where(['coffret_id' => $_SESSION['idCoffret']])->get();
                    $p = \giftbox\models\Prestation::where('id', '=', $idPresta)->first();
                    $idCat[] = $p->cat_id;
                }
            }

            $min = min($idCat);
            $max = max($idCat);
            if ($min == $max) {

                \giftbox\controllers\CoffretController::payementMauvaisNbCat();
            } else {
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    $_SESSION['erreurs'] = array();
                    if (empty($_POST["nom"])) {
                        $_SESSION["nom"] = "";
                        if (empty($_POST['input'])) {
                            $_SESSION['erreurs']['nom'] = "Le nom doit être renseigné";
                        } else {
                            $_SESSION['erreurs']['nom'] = "";
                        }
                    } else {
                        $_SESSION['nom'] = $_POST['nom'];
                        // Vérification que le nom ne contient que des lettres et des espaces
                        if (!preg_match("/^[a-zA-Z ]*$/", $_POST["nom"])) {
                            $_SESSION['erreurs']['nom'] = "S euls les lettres et les espaces sont autorisés";
                        }
                    }

                    if (empty($_POST["prenom"])) {
                        $_SESSION["prenom"] = "";
                        if (empty($_POST['input'])) {
                            $_SESSION['erreurs']['prenom'] = "Le prénom doit être renseigné";
                        } else {
                            $_SESSION['erreurs']['prenom'] = "";
                        }
                    } else {
                        $_SESSION['prenom'] = $_POST['prenom'];
                        // Vérification que le nom ne contient que des lettres et des espaces
                        if (!preg_match("/^[a-zA-Z ]*$/", $_POST["prenom"])) {
                            $_SESSION['erreurs']['prenom'] = "Seuls les lettres et les espaces sont autorisés";
                        }
                    }

                    if (empty($_POST["mail"])) {
                        $_SESSION["mail"] = "";
                        if (empty($_POST['input'])) {
                            $_SESSION['erreurs']['mail'] = "L'adresse mail doit être renseigné";
                        } else {
                            $_SESSION['erreurs']['mail'] = "";
                        }
                    } else {
                        // Vérification que le mail possède un format correct
                        if (!filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL)) {
                            $_SESSION["mail"] = "";
                            $_SESSION['erreurs']['mail'] = "Le format du mail est erroné";
                        } else {
                            $_SESSION['mail'] = $_POST['mail'];
                        }
                    }


                    if (empty($_POST['commentaire'])) {
                        $_SESSION['commentaire'] = "";
                    } else {
                        $_SESSION['commentaire'] = $_POST['commentaire'];
                    }


                    if (empty($_POST["paiement"])) {
                        $_SESSION["paiement"] = "";
                        if (empty($_POST['input'])) {
                            $_SESSION['erreurs']['paiement'] = "Un mode de paiement doit être sélectionné";
                        } else {
                            $_SESSION['erreurs']['paiement'] = "";
                        }
                    } else {
                        $_SESSION['paiement'] = $_POST["paiement"];
                    }

                    if (empty($_SESSION['erreurs'])) {
                        \giftbox\controllers\ValidationCoffretController::afficherRecapitulatif($_SESSION['nom'], $_SESSION['prenom'],
                            $_SESSION['mail'], $_SESSION['commentaire'], $_SESSION['paiement'], $_SESSION['erreurs']);
                    } else {
                        \giftbox\controllers\ValidationCoffretController::afficherFormulaire($_SESSION['nom'], $_SESSION['prenom'],
                            $_SESSION['mail'], $_SESSION['commentaire'], $_SESSION['paiement'], $_SESSION['erreurs']);
                    }
                } else {
                    \giftbox\controllers\ValidationCoffretController::afficherFormulaire();
                }
            }


        }
    }

    public
    static function afficherFormulaire($n = "", $pn = "", $m = "", $c = "", $p = "", $erreurs = null)
    {
        $vue = new vueValidationCoffret($n, $pn, $m, $c, $p, $erreurs);
        $vue->render(1); // FORMULAIRE_VIEW
    }

    public
    static function afficherRecapitulatif($n = "", $pn = "", $m = "", $c = "", $p = "", $erreurs = null)
    {
//        $prestationCoffret = PrestaCoffret::where('coffret_id', '=', $_SESSION['idCoffret'])->get();  // TODO Changer le nom de la variable ?
//
//        $prestations = array();
//        $quantite = array();
//        foreach ($prestationCoffret as $pc) {
//            $presta = \giftbox\models\Prestation::where('id', '=', $pc->presta_id)->first();
//            $quantite[] = $pc->quantite;
//            $prestations[] = $presta;
//        }

        $prestations = array();
        $quantite = array();
        foreach ($_SESSION['panier'] as $idPresta => $qte) {
            $prestations[] = $presta = \giftbox\models\Prestation::where('id', '=', $idPresta)->first();
            $quantite[] = $qte;
        }


        $vue = new vueValidationCoffret($n, $pn, $m, $c, $p, $erreurs, $prestations, $quantite);
        $vue->render(2); // RECAPITULATIF_VIEW
    }


//    public
//    static function afficherPayementClassique()
//    {
//        print 'Paiement classique';
//    }
}