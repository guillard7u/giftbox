<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 11/01/2017
 * Time: 14:24
 */

namespace giftbox\controllers;

require_once 'vendor/autoload.php';
use giftbox\models\Coffret;
use giftbox\view\VueLien;
use /** @noinspection PhpUndefinedNamespaceInspection */
    Illuminate\Database\Capsule\Manager as DB;


if (!isset($_SESSION)) {
    session_start();
}



class GenerationUrlController
{
    public static function genererUrlCoffret()
    {
        if (isset($_SESSION['idCoffret'])) {
            $hash = password_hash($_SESSION['idCoffret'], PASSWORD_DEFAULT, ['cost' => 12]);
            $hashSuivi = password_hash('gestion'.$_SESSION['idCoffret'], PASSWORD_DEFAULT, ['cost' => 12]);
            while (strpos ( $hash, '/') != false)
            {
                $hash = password_hash($_SESSION['idCoffret'], PASSWORD_DEFAULT, ['cost' => 12]);
            }
            while (strpos ( $hashSuivi, '/') != false)
            {
                password_hash('gestion'.$_SESSION['idCoffret'], PASSWORD_DEFAULT, ['cost' => 12]);
            }

            $url = 'https://webetu.iutnc.univ-lorraine.fr/www/guillard7u/PHP/giftbox/coffret/' . $hash;
            $urlSuivi = 'https://webetu.iutnc.univ-lorraine.fr/www/guillard7u/PHP/giftbox/suivi/' . $hashSuivi;


            //print "Voici l'url à fournir au destinataire du coffret <br>";
            //print '<a href = '.$url.'>'.$url.'<a><br><br>';
            //print "Voici votre url de suivi <br>";
            //print '<a href = '.$urlSuivi.'>'.$urlSuivi.'<a>';


            $vue = new VueLien();
            $vue->render($url, $urlSuivi);
        }
    }
}