-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 03 Janvier 2017 à 09:31
-- Version du serveur :  5.1.73
-- Version de PHP :  5.4.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `guillard7u`
--

-- --------------------------------------------------------

--
-- Structure de la table `prestacoffret`
--

CREATE TABLE IF NOT EXISTS `prestacoffret` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coffret_id` int(11) NOT NULL,
  `presta_id` int(11) NOT NULL,
  `quantite` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `prestacoffret`
--

INSERT INTO `prestacoffret` (`id`, `coffret_id`, `presta_id`, `quantite`) VALUES
(2, 1, 2, 1),
(3, 1, 21, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
