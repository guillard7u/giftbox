Lien vers le site :
[Giftbox on WebEtu](https://webetu.iutnc.univ-lorraine.fr/www/guillard7u/PHP/giftbox/)


# README #
* Authors:
* Comandini Mathieu
* Guillard Quentin
* Durand Felix



* Repository du Projet GiftBox


Fonctionnalités :

* 1 Afficher la liste des prestations 
**Fonctionnel**
* 2 Afficher le détail d'une prestation
**Fonctionnel**
* 3 liste de prestations par catégories
**Fonctionnel**
* 4 liste de catégories
**Fonctionnel**
* 5 Tri par prix
**Fonctionnel**
* 6 Notation des prestations
**Partiel -> affichage des notes lorsque ces dernières sont déjàs dans la base de données**
* 7 Notes moyennes
**Fonctionnel**
* 8 Page d'accueil
**Fonctionnel**
* 9 ajout de prestations dans le coffret
**Fonctionnel**
* 10 affichage d'un coffret
**Fonctionnel**
* 11 validation d'un coffret
**Fonctionnel**
* 12 paiement classique
**Fonctionnel**
* 13 ajout d'un mot de passe de gestion du coffret
**Non Abordé**
* 14 modification d'un coffret
**Fonctionnel**
* 15 création d'une cagnotte
**Partiel, erreur de réalisation -> erreur de communication dûe à une mauvaise compréhension**
* 16 participer à la cagnotte
**Partiel, erreur de réalisation -> erreur de communication dûe à une mauvaise compréhension**
* 17 cloturer la cagnotte
**Partiel, erreur de réalisation -> erreur de communication dûe à une mauvaise compréhension**
* 18 génération de l'URL à offrir
**Fonctionnel**   
***Utilisation de hashage pour la génération de l'url la page est très longue à charger, il faut parfois la rafraichir pour obtenir le lien (limite de 15 secondes sur le serveur) ***
* 19 ouverture du cadeau
**Fonctionnel**   
***Utilisation de hashage pour la génération de l'url la page est très longue à charger, il faut parfois la rafraichir pour obtenir le lien (limite de 15 secondes sur le serveur) ***
* 20 suivi du coffret cadeau
**Fonctionnel**
* 21 ajout de prestations
**Non Abordé**
* 22 suppression de prestations
**Non Abordé**
* 23 desactivation de prestations
**Non Abordé**
* 24 authentification des gestionnaires
**Non Abordé**


![FreeGreatPicture.com-21128-gift-box.jpg](https://bitbucket.org/repo/bpr8RL/images/516115247-FreeGreatPicture.com-21128-gift-box.jpg)