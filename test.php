<?php
/**
 * Created by PhpStorm.
 * User: guillard7u
 * Date: 10/01/2017
 * Time: 10:52
 */

require_once 'vendor/autoload.php';
print '<!DOCTYPE html>
                <html>
                <head> 
                    <title>Catalogue</title> 
                    <meta charset="utf-8">
                    <link href=\'https://fonts.googleapis.com/css?family=Roboto:400,300,100\' rel="stylesheet">
                    <link rel="stylesheet" href="./web/css/style.css">
                    <link rel="stylesheet" href="./web/css/bootstrap.css">
                    <link rel="stylesheet" href="./web/css/bootstrap-theme.css">
                    
                </head>
                <body>
                
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                
                   <form class="credit-card">
                        <div class="form-header">
                            <h4 class="title">Données de la carte bancaire</h4>
                        </div>
                        <div class="form-body">
                     
                            <!-- Card Number -->
                            <input type="text" class="card-number" placeholder="Numéro de carte">
                     
                            <!-- Date Field -->
                            <div class="date-field">
                              <div class="month">
                                <select name="Month">
                                  <option value="01">01 - Janvier</option>
                                  <option value="02">02 - Février</option>
                                  <option value="03">03 - Mars</option>
                                  <option value="04">04 - Avril</option>
                                  <option value="05">05 - Mai</option>
                                  <option value="06">06 - Juin</option>
                                  <option value="07">07 - Juillet</option>
                                  <option value="08">08 - Août</option>
                                  <option value="09">09 - Septembre</option>
                                  <option value="10">10 - Octobre</option>
                                  <option value="11">11 - Novembre</option>
                                  <option value="12">12 - Décembre</option>
                                </select>
                              </div>
                              <div class="year">
                                <select name="Year">
                                  <option value="2017">2017</option>
                                  <option value="2018">2018</option>
                                  <option value="2019">2019</option>
                                  <option value="2020">2020</option>
                                  <option value="2021">2021</option>
                                  <option value="2022">2022</option>
                                  <option value="2023">2023</option>
                                  <option value="2024">2024</option>
                                  <option value="2025">2025</option>
                                </select>
                              </div>
                            </div>
                     
                            <!-- Card Verification Field -->
                            <div class="card-verification">
                                <div class="cvv-input">
                                    <input type="text" placeholder="CVV">
                                </div>
                                <div class="cvv-details">
                                    <p>Code CVV se trouvant au dos de la carte</p>
                                </div>
                            </div>
                     
                            <!-- Buttons -->
                            <button type="submit" class="proceed-btn"><a href="#">Proceed</a>
                            </button>
                        </div>
                    </form>
               
                </body>
                </html>





';